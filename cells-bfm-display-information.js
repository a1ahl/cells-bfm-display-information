{
  const {
    html,
  } = Polymer;
  /**
    `<cells-bfm-display-information>` Description.

    Example:

    ```html
    <cells-bfm-display-information></cells-bfm-display-information>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-bfm-display-information | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsBfmDisplayInformation extends Polymer.Element {

    static get is() {
      return 'cells-bfm-display-information';
    }

    static get properties() {
      return {};
    }

    static get template() {
      return html `
      <style include="cells-bfm-display-information-styles cells-bfm-display-information-shared-styles"></style>
      <slot></slot>
      
          <p>Welcome to Cells</p>
      
      `;
    }
  }

  customElements.define(CellsBfmDisplayInformation.is, CellsBfmDisplayInformation);
}